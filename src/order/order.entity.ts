export class OrderEntity {
    id: number;
    client: string;
    date: Date;
    address: string;
}