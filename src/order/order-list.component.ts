import { OnInit, Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { OrderEntity } from './order.entity';
import { OrderService } from './order.service';


@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
})
export class OrderListComponent implements OnInit {
    orders: OrderEntity[];

    constructor(
        private readonly orderService: OrderService,
        private readonly confirmationService: ConfirmationService,
    ) { }

    ngOnInit() {
        this.orderService
            .getOrders()
            .then(orders => this.orders = orders);
    }

    editOrder(order: OrderEntity) {
      alert(JSON.stringify(order));
    }

    deleteOrder(order: OrderEntity) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete this order?',
            accept: async () => {
                await this.orderService.deleteOrder(order.id);
            }
        });
    }
}
