import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

import { OrderListComponent } from './order-list.component';
import { OrderService } from './order.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    OrderListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TableModule,
    ButtonModule,
    BrowserAnimationsModule,
    ConfirmDialogModule
  ],
  providers: [
    OrderService,
    ConfirmationService
  ],
  exports: [
    OrderListComponent
  ]
})
export class OrderModule { }
