import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderEntity } from './order.entity';

@Injectable()
export class OrderService {
    private readonly baseUrl = 'http://localhost:8080';

    constructor(private readonly http: HttpClient) {}

    createOrder(order: OrderEntity) {
        return this.http.post(this.baseUrl + '/order', order)
            .toPromise();
    }

    updateOrder(order: OrderEntity) {
        return this.http.put(this.baseUrl + '/order', order)
            .toPromise();
    }

    deleteOrder(orderId: number) {
        return this.http.delete(this.baseUrl + '/order/' + orderId)
            .toPromise();
    }

    getOrders(): Promise<Array<OrderEntity>> {
        return this.http.get<Array<OrderEntity>>(this.baseUrl + '/orders')
            .toPromise();
    }

    getOrder(orderId: number) {
        return this.http.get<OrderEntity>(this.baseUrl + '/order/' + orderId)
            .toPromise();
    }
}
